import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Calc {
    private Scanner scanner = new Scanner(System.in);
    private double a;
    private double b;
    private String operation;

    public void readOperation() {
        System.out.println("Введите оператор");
        operation = scanner.next();

    }

    public void readA() {
        System.out.println("a=");
        try {
            a = scanner.nextDouble();
        } catch (InputMismatchException e) {
            throw new NotNumberException("Введите числовое значение");
        }
    }

    public void readB() {
        System.out.println("b=");
        b = scanner.nextDouble();
    }

    public void divAB() {

        System.out.println(a / b);
        if (b == 0) {
            throw new RuntimeException("divAB Деление на 0");
        }
    }

    public void mulAB() {

        System.out.println(a * b);
    }

    public void addAB() {

        System.out.println(a + b);
    }

    public void subAB() {

        System.out.println(a - b);
    }

    public void process() {
        switch (operation) {
            default:
                throw new RuntimeException("Неизвестный оператор");
            case "+":
                addAB();
                break;
            case "-":
                subAB();
                break;
            case "*":
                mulAB();
                break;
            case "/":
                divAB();
                break;
        }
    }


    public static void main(String[] args) {
        Calc calc = new Calc();
        calc.readA();
        calc.readOperation();
        calc.readB();
        calc.process();
    }
}
